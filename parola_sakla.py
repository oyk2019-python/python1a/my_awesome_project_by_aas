#Parolar Dizisi
parolalar = []

while True:
    print('Bir işlem seçin')
    print('1- Parolaları Listele')
    print('2- Yeni Parola Kaydet')
    #islem adinda bir input al.
    islem = input('Ne Yapmak İstiyorsun :')
    #islem digit mi kontrol et.
    if islem.isdigit():
        #islem digitse integer a cevir.
        islem_int = int(islem)
        #islem 1 ya da 2 ye esil degilse.
        if islem_int not in [1, 2]:
            print('Hatalı işlem girişi')
            continue
        #islem 2 ise.
        if islem_int == 2:
            #web sitesi ya da girdi ismini inputla al
            girdi_ismi = input('Bir girdi ismi ya da web sitesi adresi girin :')
            #kullanici adini al
            kullanici_adi = input('Kullanici Adi Girin :')
            #parolayi al
            parola = input('Parola :')
            #parolayi bir daha al
            parola2 = input('Parola Yeniden :')
            #eposta al
            eposta = input('Kayitli E-posta :')
            #gizli soru nun cevabini al
            gizlisorucevabi = input('Gizli Soru Cevabı :')
            #kullanici adinda bosluk varsa
            if kullanici_adi.strip() == '':
                print('kullanici_adi girmediz')
                continue
            #parola da bosluk varsa
            if parola.strip() == '':
                print('parola girmediz')
                continue
            #parola 2 de bosluk varsa
            if parola2.strip() == '':
                print('parola2 girmediz')
                continue
            #eposta da bosluk varsa
            if eposta.strip() == '':
                print('eposta girmediz')
                continue
            #gizlisoruda bosluk varsa
            if gizlisorucevabi.strip() == '':
                print('gizlisorucevabi girmediz')
                continue
            #girdi isminde bosluk varsa
            if girdi_ismi.strip() == '':
                print('Girdi ismi girmediz')
                continue
            #parola, parola 2 ye esit degilse.
            if parola2 != parola:
                print('Parolalar eşit değil')
                continue
            # yeni_girdi isimli dictionary'e alinan inputlari gir.
            yeni_girdi = {
                'girdi_ismi': girdi_ismi,
                'kullanici_adi': kullanici_adi,
                'parola': parola,
                'eposta': eposta,
                'gizlisorucevabi': gizlisorucevabi,
            }
            #parolalar dizisine yeni_girdi dictionary'sini ekle.
            parolalar.append(yeni_girdi)
            continue
        #islem 1 e esitse
        elif islem_int == 1:
            alt_islem_parola_no = 0
            for parola in parolalar:
                alt_islem_parola_no += 1
                #parolalar dizisinde ki parolalari bastir.
                print('{parola_no} - {girdi}'.format(parola_no=alt_islem_parola_no, girdi=parola.get('girdi_ismi')))
                #istenilen parolanin numarasini al..
            alt_islem = input('Yukarıdakilerden hangisi ?: ')
            #digit mi degil mi kontol et
            if alt_islem.isdigit():
                #digitse, alt islem 1 den kucuk ve parolalar dizinin uzunlugundan 1 eksikten buyukse.
                if int(alt_islem) < 1 and len(parolalar) - 1 < int(alt_islem):
                    print('Hatalı parola seçimi')
                    continue
                #istenilen parolayi parola degiskenine at.
                parola = parolalar[int(alt_islem) - 1]
                print('{kullanici}\n{parola}\n{gizli}\n{eposta}'.format(
                    kullanici=parola.get('kullanici_adi'),
                    parola=parola.get('parola'),
                    eposta=parola.get('eposta'),
                    gizli=parola.get('gizlisorucevabi'),
                ))
                continue

    print('Hatalı giriş yaptınız')
